import React from "react"
import {
  Switch,
  Route
} from "react-router-dom"

import CharacterList from './components/CharacterList'
import CharacterDetail from './components/CharacterDetail'
import Error from './components/Error'
import Header from './components/Header'


/**
 * App
 * @ignore
 */
class App extends React.Component {
  render() {
    return (
      <div style={{backgroundColor: "#24282a", color: "white"}}>
        <Header/>
        <Switch>
          <Route exact path="/" component={CharacterList} />
          <Route path="/character/:id" component={CharacterDetail} />
          <Route component={Error} />
        </Switch>
    </div>
    )
  }
}

export default App