import React from "react"
import { Card, ListGroup, ListGroupItem } from "react-bootstrap"
import {Link} from "react-router-dom"


class CharacterListItem extends React.Component {
  render() {

    return (
      <div className="m-3">
        <Card style={{backgroundColor: "#505050", maxWidth: "300px"}}>
          <Card.Img variant="top" src={this.props.character.image} />
          <Card.ImgOverlay style ={{marginTop: "235px"}} className="h-25 d-flex justify-content-center">
           <h2 style={{textShadow: "0 0 2px black"}}>{this.props.character.name}</h2>
          </Card.ImgOverlay>   
          <ListGroup>
            <ListGroupItem className="d-flex justify-content-between" style={{backgroundColor: "#505050"}}><b>Status:</b><span>{this.props.character.status}</span></ListGroupItem>
            <ListGroupItem className="d-flex justify-content-between" style={{backgroundColor: "#505050"}}><b>Species:</b><span>{this.props.character.species}</span></ListGroupItem>
            <ListGroupItem className="d-flex justify-content-between" style={{backgroundColor: "#505050"}}><b>Gender:</b><span>{this.props.character.gender}</span></ListGroupItem>
            <ListGroupItem className="d-flex justify-content-between" style={{backgroundColor: "#505050"}}><b>Origin:</b> <span>{this.props.character.origin.name}</span></ListGroupItem>
            <ListGroupItem className="d-flex justify-content-between" style={{backgroundColor: "#505050"}}><b>Location:</b> <span>{this.props.character.location.name}</span></ListGroupItem>
          </ListGroup>

          <Link to={`/character/${this.props.character.id}`}>
            <Card.Footer style={{textAlign: "center", fontWeight: "bold", backgroundColor: "#333", cursor: "pointer", color:"white"}}>
              View Profile
            </Card.Footer>
          </Link>
        </Card>
      </div>

    )
  }
}

export default CharacterListItem