import React from "react"
import { Card, ul, li} from "react-bootstrap"
import "./CharacterDetail.css"

class CharacterDetail extends React.Component {
  constructor(props) {
    super(props)
    this.state  = {
      id: props.match.params.id,
      character: null,
      location: null,
      loading: true
    }
  }

  componentDidMount() {
    fetch(`https://rickandmortyapi.com/api/character/${this.state.id}`)
    .then(res => {return res.json()})
    .then(data => {
      this.setState({
        character: data
      })
      fetch(this.state.character.location.url)
      .then(res => {
       return res.json()
      }).then(data => {
        this.setState({
          location: data,
          loading: false
        })
      })
    }) 
  }

  render() {
    if(this.state.loading){
      return (<div></div>)
    }
    return (
      <div className="d-flex justify-content-center">
        <Card className="mt-3" style={{backgroundColor: "#505050", width: "80%"}}>
          <div style={{position: "relative"}}>
            <img className="profile" src={`https://rickandmortyapi.com/api/character/avatar/${this.state.id}.jpeg`} alt="" />
            <div style={{display: "inline-block", position: "absolute", bottom: "0", marginLeft:"1em"}}>
              <h1>{this.state.character.name}</h1>
              <p>Status: {this.state.character.status}</p>
            </div>
          </div>
          <hr className="mt-0"></hr>
          <div className="mr-5">
          <ul>
            <h4>Profile</h4>
            <li><b>Gender: </b><span>{this.state.character.gender}</span></li>
            <li><b>Species: </b><span>{this.state.character.species}</span></li>
            <li><b>Origin: </b><span>{this.state.character.origin.name}</span></li>
          </ul>
          </div>
          <div className="mr-5 pb-3">
          <ul>
            <h4>Location</h4>
            <li><b>Current Location: </b><span>{this.state.location.name}</span></li>
            <li><b>Type: </b><span>{this.state.location.type}</span></li>
            <li><b>Dimension: </b><span>{this.state.location.dimension}</span></li>
          </ul>
          </div>
        </Card>
      </div>
    ) 
  }
}

export default CharacterDetail