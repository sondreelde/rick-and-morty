import React from "react"
import { Navbar } from 'react-bootstrap'
import Banner from "./../Media/banner.jpg"

class Header extends React.Component {
  render() {
    return (
      <Navbar className="justify-content-md-center p-0" style={{backgroundColor: "black"}}>
        <img
        src={Banner}
        width="70%"
        className="center"
        alt="Banner"
      />
      </Navbar>
    )
  }
}

export default Header