import React from "react"
import { CardGroup, FormControl } from "react-bootstrap"
import CharacterListItem from "./CharacterListItem"

class CharacterList extends React.Component {
  constructor() {
    super();
    this.state = {
      allCharacters: null,
      characters: null,
      search: null
    }
  }

  componentDidMount() {
    this.fetchCharacters(1)
  }

  /**
   * Fetch characters from api.
   * @param {pagenumber} page 
   */
  async fetchCharacters(page) {
    const api = "https://rickandmortyapi.com/api/character/?page="
    fetch(api+page)
    .then(res => {return res.json()})
    .then(data => {
      this.setState({
        characters: data.results,
        allCharacters: data.results
      })
    })
  }

  /**
   * Create the cards for display.
   */
  displayCards() {
    if(this.state.characters === null) {
      return
    }

    let cardGroupContent = []

    for(let key in this.state.characters){
        cardGroupContent.push(<CharacterListItem key={key} character={this.state.characters[key]}></CharacterListItem>)
      }

     return <CardGroup className="d-flex flex-wrap justify-content-center align-items-stretch text-white">{cardGroupContent}</CardGroup>

  }

  /**
   * Display what is searched for.
   * @param {search parameters} s 
   */
  search(s) {
    if(s === ""){
      this.setState({
        characters: this.state.allCharacters.slice()
      })
      return
    }
    this.setState({
      characters: null
    })
    let searched = []
    for(let key in this.state.allCharacters) {
      if(this.state.allCharacters[key].name.toLowerCase().includes(s.toLowerCase())) {
        searched.push(this.state.allCharacters[key])
      }
    }
    this.setState({
      characters: searched
    })
  }
  
  render() {
    return (
      <div style={{marginLeft: "6%", marginRight: "6%"}}>
        <div className="d-flex justify-content-center">
          <FormControl
            placeholder="Enter a character name..."
            onChange={event => this.search(event.target.value)}
            aria-describedby="inputGroup-sizing-lg"
            className="mt-3 mr-3 ml-3"
            style={{background: "#333", color: "white"}}
          />
        </div>
          {this.displayCards()}
      </div>
    )
  }
}

export default CharacterList

