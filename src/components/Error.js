import React from "react"
import {Link} from "react-router-dom"
import {Button} from "react-bootstrap"
import Image from "./../Media/error.png"


class Error extends React.Component {
  render() {
    return (
      <div className="d-flex flex-column align-items-center text-align-center">
        <Link to="/" className="m-4"><Button className="btn-success btn-lg">Back Home</Button></Link>
        <img src={Image} alt="" style={{width: "70%", height: "auto"}}>
        </img>
      </div>
    )
  }
}

export default Error